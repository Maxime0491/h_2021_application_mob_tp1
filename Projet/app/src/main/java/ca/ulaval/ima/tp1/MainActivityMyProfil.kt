package ca.ulaval.ima.tp1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*

class MainActivityMyProfil : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_my_profil)

        val myprofil  = intent.extras?.getParcelable<Profil>("MYPROFIL")

        if (myprofil != null) {
            val labelNom : TextView = findViewById(R.id.labelNom)
            val labelPrenom = findViewById <TextView>(R.id.labelPrenom)
            val labelDate : TextView = findViewById(R.id.labelDate)
            val labelIdul : TextView = findViewById(R.id.labelIdul)

            val dateNaiss = myprofil.dateNais.toString().split(' ') .joinToString(limit = 3, separator = " ", truncated = "")

            labelNom.setText(myprofil.nom)
            labelPrenom.setText(myprofil.prenom)
            labelDate.setText(dateNaiss)
            labelIdul.setText("mambo10")
        }

    }
}
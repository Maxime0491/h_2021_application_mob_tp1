package ca.ulaval.ima.tp1

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.util.*

class MainActivity : AppCompatActivity() {

    /*companion object{
        const val MYPROFIL = "myprofil"
    }*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val extButton = findViewById<Button>(R.id.extButton)
        val wvButton = findViewById<Button>(R.id.wvButton)
        val uLavalButton = findViewById<Button>(R.id.ulavalButton)
        val monProfilButton = findViewById<Button>(R.id.monProfilButton)

        var UrlToLoad : String = "https://monportail.ulaval.ca"

        fun convertDateToLong(date: String): Long {
            val df = SimpleDateFormat("yyyy.MM.dd")
            return df.parse(date).time
        }

        extButton.setOnClickListener(){
            val intent : Intent = Intent(Intent.ACTION_VIEW, Uri.parse(UrlToLoad))
            startActivity(intent)
        }

        wvButton.setOnClickListener(){
            val intent : Intent = Intent(this, MainActivityWv::class.java)
            intent.putExtra("UrlToLoad", UrlToLoad)
            startActivity(intent)
        }

        uLavalButton.setOnClickListener(){
            val intent : Intent = Intent(this, MainActivityUlaval::class.java)
            startActivity(intent)
        }

        monProfilButton.setOnClickListener(){
            val date = Date(19910407)
            val myprofil = Profil("Mbouemboue", "Maxime", date,"mambo10")
            val intent : Intent = Intent(this, MainActivityMyProfil::class.java)
            intent.putExtra("MYPROFIL", myprofil)
            startActivity(intent)
        }



    }
}
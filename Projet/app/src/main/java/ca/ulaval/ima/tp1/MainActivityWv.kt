package ca.ulaval.ima.tp1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button

class MainActivityWv : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_wv)

        val UrlToLoad : String = intent.extras?.getString("UrlToLoad").toString()
        val buttonFermerSiteWv = findViewById<Button>(R.id.buttonFermerSiteWv)
        val mywebView : WebView = findViewById(R.id.myWebView)


        mywebView.webViewClient = object  : WebViewClient(){
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                url: String
            ): Boolean {
                view?.loadUrl(url)
                return true
            }
        }

        buttonFermerSiteWv.setOnClickListener(){
            val intent : Intent = Intent(this, MainActivityWv::class.java)
            this.finish()
        }

        mywebView.loadUrl(UrlToLoad)
        mywebView.settings.javaScriptEnabled = true
        mywebView.settings.allowContentAccess = true
        mywebView.settings.useWideViewPort = true
        mywebView.settings.domStorageEnabled = true
    }
}
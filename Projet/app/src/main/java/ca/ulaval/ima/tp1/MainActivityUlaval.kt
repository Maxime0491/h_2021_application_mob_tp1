package ca.ulaval.ima.tp1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivityUlaval : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_ulaval)

        val closeBoutonUlaval = findViewById<Button>(R.id.closeBoutonUlaval)

        closeBoutonUlaval.setOnClickListener(){
            val intent : Intent = Intent(this, MainActivityUlaval::class.java)
            this.finish()
        }
    }
}
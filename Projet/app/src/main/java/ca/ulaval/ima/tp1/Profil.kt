package ca.ulaval.ima.tp1

import android.icu.util.Calendar
import android.os.Parcel
import android.os.Parcelable
import java.time.LocalDate
import java.time.LocalTime
import java.util.*
import kotlin.time.days

data class Profil  (var nom : String, var prenom : String, var dateNais : Date, var IDUL: String) : Parcelable {
    companion object {
        @JvmField
        val CREATOR = object : Parcelable.Creator<Profil> {
            override fun createFromParcel(parcel: Parcel) = Profil(parcel)
            override fun newArray(size: Int) = arrayOfNulls<Profil>(size)
        }
    }
    constructor(parcel: Parcel) : this(
            nom = parcel.readString()?: "",
            prenom = parcel.readString() ?: "",
            dateNais = Date(parcel.readLong()),
            IDUL = parcel.readString() ?: ""
    )
    override fun describeContents(): Int {
        return 0
    }
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nom)
        parcel.writeString(prenom)
        parcel.writeSerializable(dateNais)
        parcel.writeString(IDUL)
    }
}